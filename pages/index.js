import fetch from 'isomorphic-unfetch'
class Login extends React.Component {
    state = {
        firstName: "",
        lastName: "",
        email: "",
        password: ""
    }

    onSubmit = async (e) => {
        e.preventDefault()
        console.log(this.state)
        try {
            const apiData = { firstName: this.state.firstName, lastName: this.state.lastName, email: this.state.email, password: this.state.password }
            const response = await fetch("http://localhost:5000/api/users/register", {
                body: JSON.stringify({ firstName: this.state.firstName, lastName: this.state.lastName, email: this.state.email, password: this.state.password }),
                method: "POST",
                headers: {
                    "Content-type": 'application/json'
                }
            })
            const responseJson = await response.json()

            if (responseJson.error) {
                alert(responseJson.error)
            } else {
                alert("success and token is" + responseJson.token)
            }
        } catch (err) {
            console.log(err.error)
        }

    }
    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <h1>Hello Bookart</h1>
                    <h2>Login Page</h2>
                    <label>FirstName</label>

                    <br></br>
                    <input type="text" name="firstName" value={this.state.firstName} onChange={(e) => this.setState({ firstName: e.target.value })}></input>
                    <br></br>
                    <label>LastName</label>
                    <br></br>
                    <input type="text" name="lastName" value={this.state.lastName} onChange={(e) => this.setState({ lastName: e.target.value })}></input>
                    <br></br>
                    <label>Email</label>
                    <br></br>
                    <input type="email" name="email" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })}></input>
                    <br></br>
                    <label>Password</label>
                    <br></br>
                    <input type="password" name="password" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })}></input>
                    <br></br>
                    <br></br>
                    <button type="submit">sign up</button>
                </form>
            </div >
        )
    }
}

export default Login;